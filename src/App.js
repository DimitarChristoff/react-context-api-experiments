import React, {Component, Fragment} from 'react';
import Header from './Modules/Header';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {UserProvider} from './context/index'

import Dashboard from './Modules/Dashboard';
import Account from './Modules/Account';

class App extends Component {

  render(){
    return <UserProvider>
      <div className="container">
        <Router>
          <Fragment>
            <Route path="/*" component={Header}/>
            <div className="main">
              <Switch>
                <Route exact path="/" component={Dashboard}/>
                <Route path="/account" component={Account}/>
              </Switch>
            </div>
          </Fragment>
        </Router>
      </div>
    </UserProvider>;
  }
}

export default App;
