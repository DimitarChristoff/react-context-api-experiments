import React from 'react';
import {SiteConsumer, SiteProvider, UserConsumer} from "../../context";

export default props => <div className='card'>
  <div className="card-header">
    <div className="card-header-title">
      Dashboard
    </div>
  </div>
  <div className="card-content">
    <SiteProvider>
      <SiteConsumer>
        {({state: {title, time}}) => <UserConsumer>
          {({state: {name}}) => <React.Fragment>
            Hello, {name}, welcome back to <strong>{title}</strong>. It is now {time}
          </React.Fragment>}
        </UserConsumer>}
      </SiteConsumer>
    </SiteProvider>
  </div>
</div>;
