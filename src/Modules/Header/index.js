import React, {Fragment} from 'react';
import {Tab, Tabs, Button, Navbar, NavbarGroup, NavbarHeading} from '@blueprintjs/core';
import {Link} from 'react-router-dom';
import {UserConsumer} from '../../context/index';

import './Header.scss';

export default class Header extends React.Component {

  state = {
    selected: ''
  }

  setSelected(match = this.props.match){
    let selected;

    switch(match.params[0]){
      case 'account':
        selected = 'account';
        break;
      case 'dashboard':
      case '':
        selected = 'dashboard';
        break;
      default:
        selected = '';
    }
    this.setState({
      selected
    });
  }

  componentWillMount(){
    this.setSelected();
  }

  componentWillReceiveProps(newProps){
    this.setSelected(newProps.match);
  }

  render(){
    return <Fragment>
      <UserConsumer>
        {({state}) =>
        <Navbar className="pt-fixed-top">
          <div className="container">
            <NavbarGroup align="left">
              <NavbarHeading>React Bollocks</NavbarHeading>
            </NavbarGroup>
            <NavbarGroup align="right">
              <span className="api-logo"/>
            </NavbarGroup>
            <NavbarGroup align="right">
              <Link to='/account'>
                <Button icon="user" className="pt-minimal" text={`${state.name} ${state.surname}`}/>
              </Link>
            </NavbarGroup>
          </div>
        </Navbar>}
      </UserConsumer>
      <div className="container main-header">
        <Tabs selectedTabId={this.state.selected}>
          <Tab id="dashboard" title={<Link to='/'>Dashboard</Link>}/>
          <Tab id="account" title={<Link to='/account'>Manage account</Link>}/>
        </Tabs>
      </div>
    </Fragment>
  }
}
