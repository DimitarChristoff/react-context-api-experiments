import React, { createContext } from 'react';
import { shallow, mount } from 'enzyme';
import { Navbar, Tabs } from '@blueprintjs/core';
import Header from '../';
import { UserProvider } from '../../../context/index';

jest.mock('react-router-dom');

describe('<Header />', () => {
  const props = {
    match: {
      params: []
    }
  };
  let wrapper;

  describe('shallow', () => {
    beforeEach(() => {
      wrapper = shallow(<Header {...props} />);
    });
  
    it('renders Navbar', () => {
      // Fail - Function as a child is not evaluated
      expect(wrapper.find(Navbar)).toHaveLength(1);
    });
  
    it('renders Tabs', () => {
      // Pass
      expect(wrapper.find(Tabs)).toHaveLength(1);
    });
  });

  describe('mount', () => {
    beforeEach(() => {
      // Need to wrap component tree with the Provider (could create helpers/mountWithProvider.js)
      // Do we have a way of defining the state in the Provider on the fly? 
      // Creating a context here and mocking the Consumer is proving tricky!
      wrapper = mount(<UserProvider><Header {...props} /></UserProvider>);
    });
    
    it('renders Navbar', () => {
      // Enzyme Internal Error: unknown node with tag 13
      // Passes with these changes https://github.com/airbnb/enzyme/pull/1513
      expect(wrapper.find(Navbar)).toHaveLength(1);
    });
  
    it('renders Tabs', () => {
      // Enzyme Internal Error: unknown node with tag 13
      // Passes with these changes https://github.com/airbnb/enzyme/pull/1513
      expect(wrapper.find(Tabs)).toHaveLength(1);
    });
  });
});
