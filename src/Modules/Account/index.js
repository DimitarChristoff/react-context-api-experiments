import React, {Component, Fragment} from 'react';
import {Button} from '@blueprintjs/core';
import {UserConsumer} from '../../context';
import './Account.scss';


class Account extends Component {

  render(){
    return <UserConsumer>
      {({state, actions}) =>{
        const {name, surname} = state;

        return <Fragment>
          <div className="columns">
            <div className="column">
              <div className='card'>
                <div className="card-header">
                  <div className="card-header-title">
                    Account
                  </div>
                </div>
                <div className="card-content">
                  <div className="columns">
                    <div className="column">
                      <div className="pt-form-group">
                        <label className="pt-label" htmlFor="example-form-group-input-a">
                          Name
                        </label>
                        <div className="pt-form-content">
                          <input className="pt-input pt-fill" type="text" placeholder="Name" dir="auto" value={name} onChange={({target}) =>{
                            actions.SET_NAME(target.value)
                          }}/>
                        </div>
                        <div className="pt-form-group">
                          <label className="pt-label" htmlFor="example-form-group-input-a">
                            Surname
                          </label>
                          <div className="pt-form-content">
                            <input className="pt-input pt-fill" type="text" placeholder="Name" dir="auto" value={surname} onChange={({target}) =>{
                              actions.SET_SURNAME(target.value)
                            }}/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card-footer">
                  <div className="card-footer-item">
                    <Button
                      icon={'add'}
                      text={'Ok then'}
                      onClick={() => this.setState({})}
                    />

                  </div>
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      }}
    </UserConsumer>;
  }
}

export default Account;
