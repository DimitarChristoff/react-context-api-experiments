import React, {createContext, Component} from 'react';
import fecha from 'fecha';

const {
  Provider: UProvider,
  Consumer: UserConsumer
} = createContext();

const {
  Provider: SProvider,
  Consumer: SiteConsumer
} = createContext();

class UserProvider extends React.Component {
  state = {
    name: 'Bob',
    surname: 'Roberts'
  };

  render(){
    return <UProvider value={{
      state: this.state,
      actions: {
        SET_NAME: name => this.setState({name}),
        SET_SURNAME: surname => this.setState({surname}),
      }
    }}>{this.props.children}</UProvider>
  }
}


class SiteProvider extends Component {
  state = {
    title: 'React context test',
    time: ''
  };

  componentDidMount(){
    this.timer = this.tick()
  }

  componentWillUnmount(){
    this.stop();
  }

  stop(){
    clearTimeout(this.timer);
  }

  tick = () => {
    this.setState({time: fecha.format(new Date(), 'hh:mm:ss')});
    this.timer = setTimeout(this.tick, 1000);
  }

  render(){
    return <SProvider value={{
      state: this.state,
      actions: {}
    }}>{this.props.children}</SProvider>
  }
}


export {
  UserProvider,
  UserConsumer,
  SiteProvider,
  SiteConsumer
};
